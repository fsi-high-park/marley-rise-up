<?php
$_POST = json_decode(file_get_contents("php://input"), true);

$post = [
    'EMAIL' => $_POST['data']['email'], //EMAIL
    'COLUMN1' => $_POST['data']['firstName'], // FIRST NAME
    'COLUMN2'   => $_POST['data']['lastName'], // LAST NAME
    'COLUMN3' => $_POST['data']['month'].'/'.$_POST['data']['day'].'/'.$_POST['data']['year'],  // BIRTHDAY
    'COLUMN4' => $_POST['data']['local'], // Local
    'COLUMN6' => $_POST['data']['province'] // PROVINCE
];

include('../../db_config.php');

// Send entry to Acoustic/IBM
$query = http_build_query($post, '', '&');

$ch = curl_init($acoustic_url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $query);

// execute!
$response = curl_exec($ch);

// close the connection, release resources used
curl_close($ch);

$json_response = json_decode($response);

$return_response = ['status' => '', 'message' => ""];
if($json_response->status == true){
	$return_response = ['status' => 'true', 'message' => "Success"];
	echo(json_encode($return_response));
}else{
	$return_response = ['status' => 'false', 'message' => "IBM responded with non {status:true}."];
	echo(json_encode($return_response));
}

// Save entry into local database.
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$stmt = $conn->prepare("INSERT INTO ".$table." (firstname, lastname, email, local, province, birthday) VALUES (?, ?, ?, ?, ?, ?)");
$stmt->bind_param("ssssss", $firstname, $lastname, $email, $local, $province, $birthday);

$firstname = $post['COLUMN1'];
$lastname = $post['COLUMN2'];
$email = $post['EMAIL'];
$local = $post['COLUMN4'];
$province = $post['COLUMN6'];
$birthday = $post['COLUMN3'];

$stmt->execute();

$stmt->close();
$conn->close();

?>
