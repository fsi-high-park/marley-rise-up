import React from "react"
import { Location } from '@reach/router';

import { Link } from "gatsby"

import black_logo from "../assets/images/marley-rise-up-logo@2x.png"
import facebook_logo from "../assets/images/facebook-logo-button.png"
import twitter_logo from "../assets/images/twitter-logo-button.png"
import instagram_logo from "../assets/images/instagram.png"

export default class nav extends React.Component {
    ActivePage(location, this_page){
        if(location.pathname === this_page){
            return 'active';
        }else{ return ""; }
    }
    render() {
        return(
            <Location>
            {({ location }) => (
                <nav className="navbar navbar-expand-lg navbar-light">
                    <Link aria-label="Home" to="/" className="navbar-brand" ><img src={black_logo} alt="Marley Rise Up logo"/></Link>
                    <div className="mb-3 mb-md-0" id="navbarNav">
                        <span>© Copyright 2019</span>
                        <ul className="navbar-nav">
                            <li className={"nav-item "+this.ActivePage(location, '/')}>
                                <Link className="nav-link" to="/">Home</Link>
                            </li>
                            <li className={"nav-item "+this.ActivePage(location, '/privacy-policy/')}>
                                <Link className="nav-link" to="/privacy-policy/">Privacy Policy</Link>
                            </li>
                            <li className={"nav-item "+this.ActivePage(location, '/terms-of-use/')}>
                                <Link className="nav-link" to="/terms-of-use/">Terms of Use</Link>
                            </li>
                            <li className={"nav-item "}>
                                <a className="nav-link" href="mailto:service@highparkcompany.com">Contact Us</a>
                            </li>
                        </ul>  
                    </div>
                    <ul className="list-unstyled d-none d-md-flex invisible social-links mb-4 mb-md-0">
                        <li>
                            <a><img src={instagram_logo} alt="" /></a>
                        </li>
                        <li>
                            <a><img src={facebook_logo} alt="" /></a>
                        </li>
                        <li>
                            <a><img src={twitter_logo} alt="" /></a>
                        </li>                        
                    </ul>
                </nav>
            )}
            </Location>
        )   
    }
}