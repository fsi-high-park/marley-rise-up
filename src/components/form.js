import React from "react"
import axios from 'axios';
import { navigate } from "gatsby"


import Province from "../components/age-components/province"
import Month from "../components/age-components/month"
import Day from "../components/age-components/day"
import Year from "../components/age-components/year"

/*Assets*/
import white_logo from "../assets/images/marley-rise-up-logo-wht@2x.png"

export default class form extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName:"",
            lastName:"",
            email:"",
            newsletter:"",
            month:"",
            day:"",
            year:"",
            province:"",
            local:"en",
            formErrors: { firstName:false, lastName:false, email:false, province: false, month: false, day: false, year: false, age: false, newsletter:false,qc: false, email_valid:false},
            formValid: false
        }
    }
    componentDidMount(){
        let age = window.sessionStorage.getItem('age');
        if(age){
            age = JSON.parse(age);
            this.setState({month:age.month, day:age.day, year:age.year, province:age.province});
        }
    }
    handleInputChange = event => {
        const target = event.target
        const value = target.value
        const name = target.name
        this.setState({
          [name]: value,
        })
    }
    handleSubmit = event => {
        event.preventDefault();
        let form_details = {
            firstName:this.state.firstName,
            lastName:this.state.lastName,
            email:this.state.email,
            province:this.state.province,
            month:this.state.month,
            day:this.state.day,
            year:this.state.year,
            newsletter: this.state.newsletter
        }
        var formValid = this.validateForm(form_details);
        
        if(formValid){
            this.sendFormData();
        }
    }
    validateForm(form_details){
        //Create Array of age details to loop through
        const field_keys = Object.keys(form_details);
        var tempFormErrors = this.state.formErrors;
        var formValid = true;
        
        for(var field_key of field_keys){
            //Loop through form fields, if one is left empty flag it as an error in formErrors state object

            if(form_details[field_key] === ""){
                tempFormErrors[field_key] = true;
                formValid = false;
            }else{
                if(field_key == "firstName" || field_key == "lastName"){
                    var format = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
                    if(format.test(form_details[field_key])){
                        tempFormErrors[field_key] = true;
                        formValid = false;
                    }else{
                        tempFormErrors[field_key] = false;    
                    }
                }
                else if(field_key == "email"){
                    var format = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    if(!format.test(form_details[field_key])){
                        tempFormErrors["email_valid"] = true;
                        formValid = false;
                    }else{
                        tempFormErrors["email_valid"] = false;    
                        tempFormErrors[field_key] = false;
                    }
                }
                else{
                    tempFormErrors[field_key] = false;
                }
            }
        }
        // If we have errors stop here and set the error state object.
        

        //if(!formValid){ return; }

        //Checking for legal age as per province
        //var tempFormErrors = this.state.formErrors;
        if(!tempFormErrors.month && !tempFormErrors.day && !tempFormErrors.year && !tempFormErrors.province){
            var age = this.getAge(this.state.year+"/"+this.state.month+"/"+this.state.day);
            tempFormErrors.qc = false;
            if(this.state.province === "AB"){
                if(age < 18){
                    tempFormErrors.age = true;
                    formValid = false;
                }else{
                    tempFormErrors.age = false;
                }
            }else if(this.state.province === "QC"){
                tempFormErrors.qc = true;
                formValid = false;
            }else{
                if(age < 19){
                    tempFormErrors.age = true;
                    formValid = false;
                }else{
                    tempFormErrors.age = false;
                }
            }
        }
        // Set the error state object.
        this.setState({formErrors: tempFormErrors});
        
        // Check if form is valid. If so return true
        return formValid;
        
    }
    sendFormData(){        
        axios.post('/api/post.php', {data: this.state})
        .then(res => {            
            navigate("/thank-you/")
        })
    }
    isValid(field_key){
        if(this.state.formErrors[field_key]){
            return "error ";
        }else{
            return "";
        }
    }
    getAge(dateString) {
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }
    render() {
        return(
            <div id="entry-form" className="py-4 ">
                <form className="mb-2 mb-md-5" onSubmit={this.handleSubmit}>
                    <img className="mw-100 mb-4 mt-5" src={white_logo} alt="Marley Rise Up logo"/>
                    <p className="mb-4 text-center">Believe in the power of the human spirit and join our movement.<br/>
Plus, stay up to date with all things Rise&nbsp;Up. </p> 
                    <p className="small"><sup>*</sup>Required Field</p>
                    <div className="form-row">
                        <div className={"col-md-6 form-group " + this.isValid("firstName")}>
                            <input type="text" className="form-control" id="lead_firstName" name="firstName" value={this.state.firstName} onChange={this.handleInputChange} />
                            <label htmlFor="lead_firstName"><sup>*</sup>First Name</label>
                            <small className="error-message"><sup>*</sup>Please enter your first name.</small>
                        </div>
                        <div className={"col-md-6 form-group " + this.isValid("lastName")}>
                            <input type="text" className="form-control" id="lead_lastName" name="lastName" value={this.state.lastName} onChange={this.handleInputChange}  />
                            <label htmlFor="lead_lastName"><sup>*</sup>Last Name</label>
                            <small className="error-message"><sup>*</sup>Please enter your last name.</small>
                        </div>
                    </div>
                    
                    <div className={"form-group " + this.isValid("email") + this.isValid("email_valid")}>
                        
                        <input type="text" className="form-control" id="lead_email" name="email" value={this.state.email} onChange={this.handleInputChange}  />
                        <label htmlFor="lead_email"><sup>*</sup>Email Address</label>
                        {this.isValid("email_valid") === "error " ? (
                            <small className={"error-message"}><sup>*</sup>Please enter a valid email address.</small>  
                        ) : (
                            <small className="error-message"><sup>*</sup>Please enter your email address.</small>
                        )}                        
                        
                    </div>
                    
                    <div className="form-row">
                        <div className="col-lg-6">
                            <div className={"form-group " + this.isValid("province")}>
                                <Province id="lead_province" province={this.state.province} name="province" handleInputChange={this.handleInputChange}/>
                                <label htmlFor="lead_province"><sup>*</sup>Province/Territory</label>                                
                                <small className="error-message">*Select your Province/Territory.</small>                                                               
                            </div>
                        </div>
                        <div className="col-lg-6">
                            <div className="form-row">
                                <div className={"mb-4 mb-md-0 col-md-6 form-group " + this.isValid("month") + this.isValid("day") + this.isValid("year")}>
                                    <Month id="lead_month" month={this.state.month} name="month" handleInputChange={this.handleInputChange}/>
                                    <label className="d-none d-md-block" htmlFor="lead_month"><sup>*</sup>Date of Birth</label>
                                </div>
                                <div className={"mb-4 mb-md-0 col-md-3 form-group " + this.isValid("day")}>
                                    <Day id="lead_day"  month={this.state.month} day={this.state.day} name="day" handleInputChange={this.handleInputChange}/>
                                </div>
                                <div className={"mb-4 mb-md-0 col-md-3 form-group " + this.isValid("year")}>
                                    <Year id="lead_year" year={this.state.year} name="year" handleInputChange={this.handleInputChange}/>
                                    <label className="d-block d-md-none mt-2" htmlFor="lead_month"><sup>*</sup>Date of Birth</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p className={"age-message text-center "+this.isValid("age")}><small>Unfortunately, you are too young to receive communications from us. Come back and visit us when you’re the age of majority in your Province/Territory.</small></p>
                    <p className={"age-message text-center "+this.isValid("qc")}><small>Our apologies. Right now, our content is not available in your location.</small></p>

                    <div className={"form-group mt-4 " + this.isValid("newsletter")}>
                        <div className="form-check">
                        <input className="form-check-input" type="checkbox" id="gridCheck" name="newsletter" checked={this.state.newsletter} onChange={this.handleInputChange} />
                        <label className="form-check-label" htmlFor="gridCheck">
                        YES! I want to receive electronic messages from Rise&nbsp;Up. You can unsubscribe at any time.
                        </label>
                        <small className="error-message"><sup>*</sup>Please confirm you would like to receive email communications.</small>
                        </div>
                    </div>
                    <button type="submit" className="mt-4 mt-md-4 mb-1 mb-md-0 btn btn-primary">Subscribe</button>
                </form>
            </div>
        )
    }
}