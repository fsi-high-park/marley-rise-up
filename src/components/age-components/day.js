import React from "react"
export default class day extends React.Component {
    renderOptions(){
        let optionsDOM = [];

        var days_in_month = 31;
        if(this.props.month == '2'){
            days_in_month = 29;
        }else if(this.props.month == '4' || this.props.month == '6' || this.props.month == '9' || this.props.month == '11'){
            days_in_month = 30;
        }
        for (let index = 1; index <= days_in_month; index++) {
            optionsDOM.push(<option key={index} value={index}>{index}</option>)
        }
        return optionsDOM;
    }
    render() {
        return(
            <select className="form-control" id={this.props.id} name={this.props.name} value={this.props.day} onChange={this.props.handleInputChange}>
                <option selected="true" disabled>Day</option>
                {this.renderOptions()}
            </select>
        )
    }
}