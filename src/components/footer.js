import React from "react"
import Nav from "./nav"
import Helmet from 'react-helmet'


export default () => (
    <footer className="footer pt-3">
        <Nav/>
        <Helmet>
            <title>Marley Rise Up</title>
            <link rel="icon" type="image/png" href="/favicon.ico" sizes="16x16" />
            <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png" />
            <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
            <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
            <link rel="icon" type="image/png" sizes="192x192" href="/android-chrome-192x192.png"/>
            <link rel="icon" type="image/png" sizes="512x512" href="/android-chrome-512x512.png"/>
            <link rel="manifest" href="/site.webmanifest" />
            <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5" />
            <script>
            {`
                (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                })(window,document,'script','dataLayer','GTM-5RW7NLD');
            `}
            </script>
        </Helmet>
        <noscript>{`<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5RW7NLD"
height="0" width="0" style="display:none;visibility:hidden">`}</noscript>
    </footer>
)